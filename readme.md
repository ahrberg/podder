# podder

`podder` is a bash script intended for downlaod of Podcasts and possibly syncing them to an external device.

## Usage

`podder` uses a file `subscribe_list.txt` with rss urls to the podcasts to download episodes from. `podder` places a `.history` file with previously downloaded episodes. This ensure to not downlaod episodes again and also episodes can be deleted from the external device and not be synced again.

Example of subscribe_list.txt

```
https://tuxdigital.com/feed/thisweekinlinux-mp3

```

In the folder where the `subscribe_list.txt` is places you can run the `fetch` and the `sync` commands.

### `podder fetch`

Fetch episodes from `subscribe_list.txt`

### `podder sync /mnt/mydevice`

Sync episodes to device. Will be default delete files locally after sync.

The device location can also be stored in the `.podder` file as `SYNC_DIR=/mnt/mydevice`.

## Dependencies

You need to have/install these tools

- xmlstarlet
- curl
- rsync
- wget
